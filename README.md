# Arrays en JavaScript

Manejar arrays en javascript es muy sencillo debido a los métodos que ya se encuentran integrados.

Todo el código mostrado puede encontrarse [aquí](https://gitlab.com/manticore-labs/open-source/investigacion/arrays-javascript)

Para este ejemplo utilizaremos un array simple.

``` JavaScript
var arregloNumeros = [1, 2, 3, 4, 5];
```

## Práctica

Primero veamos lo que nos imprimen los siguientes comandos. 

### indexOf, push y posiciones

``` JavaScript
console.log(arregloNumeros);
// Devuelve el indice del elemento.
console.log(arregloNumeros.indexOf(7))
// Agrega un elemento al final del array
arregloNumeros.push(6)
console.log(arregloNumeros)
// Quita el ultimo elemnto al final del array y lo retorna.
arregloNumeros.pop()
console.log(arregloNumeros)
// Toma el elemento en la posición especificada.
console.log(arregloNumeros[0])

```

#### _Salida_
![Propiedades iniciales de arrays](./imagenes/consolelogs1.png)

Como podemos observar el numero 7 no existe por lo tanto

``` JavaScript
arregloNumeros.indexOf(7)
```
devuelve -1. Si existiera devolvería el índice del número que especificamos.

### Splice

El operador splice remueve elementos de un array y los retorna.

``` JavaScript
console.log('original',arregloNumeros)
var arregloSplice = arregloNumeros.splice(0,2)
console.log('corte', arregloSplice)
console.log('original',arregloNumeros)
```
#### _Salida_
![Propiedad splice](./imagenes/splice.png)

Aquí podemos ver que el operador **splice** modifica el array original por lo cual, si se necesita, se debe copiar el array en otra variable para poder modificarlo. 

### For Each
El operador forEach permite recorrer cada elemento del array.
Este operador recibe un callback con parametros de valor del elemento, índice del elemento y el array completo.

``` JavaScript
arregloNumeros.forEach(function(valor, indice, arreglo){
    console.log('valor ',valor)
    console.log('indice ', indice)
    console.log('vector ',arreglo)
     });
    
```
#### _Salida_
![Propiedad for each](./imagenes/for-each.png)

### Map

El operador map permite modificar los valores de un array y retorna un array con estos valores. Recibe como parametros un callback que permite recuperar el valor del elemento, el índice de un elemento y el array completo, este operador tambien recorre cada elemento.

El siguiente 

``` JavaScript
var arregloNumeros = [1, 2, 3, 4, 5];
var operadorMap = arregloNumeros.map((valor, indice, array) => {

    return valor + 1;
})
console.log('Arreglo Original', arregloNumeros)
console.log('Arreglo Map', operadorMap)
```

### Filter
El operador filter retorna todos los elementos del array que cumplen con la condición del callback, este operador tambien recorre cada elemento.

**Para estos dos últimos ejemplos utilizaremos este array:**
``` JavaScript
var arregloNumeros = [1, 2, 3, 4, 5, 3];
```
Este ejemplo devuelve todos los numeros 3 que encuentre en el array.

``` JavaScript
var operadorFilter = arregloNumeros.filter((valor, indice) => {
    return valor === 3
})

console.log(operadorFilter)
```

### Find
El operador find retorna el primer elemento que cumpla con la condición especificada en el callback sino encuentra nada retorna **undefined**. Este operador no recorre todos los elementos.

#### _Salida_

``` JavaScript
var arregloNumeros = [1, 2, 3, 4, 5, 3];
var operadorFind = arregloNumeros.find((valor,indice)=>{
    return valor === 3
 })
 console.log(operadorFind)
```
<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>